<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220724125255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE uw_word_relation (id INT AUTO_INCREMENT NOT NULL, uw_id INT NOT NULL, word_id INT NOT NULL, weight INT NOT NULL, INDEX IDX_EC055F9D98F81CA5 (uw_id), INDEX IDX_EC055F9DE357438D (word_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE uw_word_relation ADD CONSTRAINT FK_EC055F9D98F81CA5 FOREIGN KEY (uw_id) REFERENCES uw (id)');
        $this->addSql('ALTER TABLE uw_word_relation ADD CONSTRAINT FK_EC055F9DE357438D FOREIGN KEY (word_id) REFERENCES word (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE uw_word_relation');
    }
}
