<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220623141846 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD company VARCHAR(255) DEFAULT NULL, ADD identification_number VARCHAR(50) DEFAULT NULL, ADD address VARCHAR(500) DEFAULT NULL, ADD zip_code VARCHAR(25) DEFAULT NULL, ADD city VARCHAR(100) DEFAULT NULL, ADD country VARCHAR(255) DEFAULT NULL, ADD last_payment_date DATETIME DEFAULT NULL, ADD automatic_renewal TINYINT(1) DEFAULT NULL, ADD vip_end_date DATETIME DEFAULT NULL, ADD next_renewal_date DATETIME DEFAULT NULL, ADD vip_extra_info LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP company, DROP identification_number, DROP address, DROP zip_code, DROP city, DROP country, DROP last_payment_date, DROP automatic_renewal, DROP vip_end_date, DROP next_renewal_date, DROP vip_extra_info');
    }
}
