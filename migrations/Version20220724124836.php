<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220724124836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE uw_word');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE uw_word (id INT AUTO_INCREMENT NOT NULL, uw_id INT NOT NULL, word_id INT NOT NULL, weight INT NOT NULL, UNIQUE INDEX UNIQ_6DEDEF9C98F81CA5 (uw_id), UNIQUE INDEX UNIQ_6DEDEF9CE357438D (word_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE uw_word ADD CONSTRAINT FK_6DEDEF9C98F81CA5 FOREIGN KEY (uw_id) REFERENCES uw (id)');
        $this->addSql('ALTER TABLE uw_word ADD CONSTRAINT FK_6DEDEF9CE357438D FOREIGN KEY (word_id) REFERENCES word (id)');
    }
}
