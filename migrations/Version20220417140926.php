<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220417140926 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, code VARCHAR(20) NOT NULL, name VARCHAR(255) NOT NULL, orig_name VARCHAR(255) NOT NULL, flag_url VARCHAR(1000) DEFAULT NULL, active TINYINT(1) NOT NULL, extra LONGTEXT DEFAULT NULL, INDEX IDX_D4DB71B5727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paradigm (id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, name VARCHAR(255) NOT NULL, forms LONGTEXT NOT NULL, INDEX IDX_ECF68BE182F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE us (id INT AUTO_INCREMENT NOT NULL, representation VARCHAR(1000) NOT NULL, tree LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE us_tag (us_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_3BF9D6AB179A8BF2 (us_id), INDEX IDX_3BF9D6ABBAD26311 (tag_id), PRIMARY KEY(us_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uw (id INT AUTO_INCREMENT NOT NULL, main_tag_id INT DEFAULT NULL, lemma VARCHAR(100) NOT NULL, definition VARCHAR(510) DEFAULT NULL, identifier VARCHAR(255) NOT NULL, cat VARCHAR(20) NOT NULL, info VARCHAR(1000) DEFAULT NULL, is_basic TINYINT(1) NOT NULL, proper_noun VARCHAR(100) DEFAULT NULL, active TINYINT(1) NOT NULL, extra LONGTEXT DEFAULT NULL, INDEX IDX_DDF07BD325CEDB07 (main_tag_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uw_tag (uw_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_CE79706B98F81CA5 (uw_id), INDEX IDX_CE79706BBAD26311 (tag_id), PRIMARY KEY(uw_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uw_word (id INT AUTO_INCREMENT NOT NULL, uw_id INT NOT NULL, word_id INT NOT NULL, weight INT NOT NULL, UNIQUE INDEX UNIQ_6DEDEF9C98F81CA5 (uw_id), UNIQUE INDEX UNIQ_6DEDEF9CE357438D (word_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE word (id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, paradigm_id INT DEFAULT NULL, lemma VARCHAR(100) NOT NULL, orig_lemma VARCHAR(100) DEFAULT NULL, variants VARCHAR(255) DEFAULT NULL, ipa VARCHAR(255) DEFAULT NULL, cat VARCHAR(20) NOT NULL, info VARCHAR(1000) DEFAULT NULL, forms LONGTEXT DEFAULT NULL, weight INT NOT NULL, active TINYINT(1) NOT NULL, extra LONGTEXT DEFAULT NULL, INDEX IDX_C3F1751182F1BAF4 (language_id), INDEX IDX_C3F1751142C869E2 (paradigm_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE language ADD CONSTRAINT FK_D4DB71B5727ACA70 FOREIGN KEY (parent_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE paradigm ADD CONSTRAINT FK_ECF68BE182F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE us_tag ADD CONSTRAINT FK_3BF9D6AB179A8BF2 FOREIGN KEY (us_id) REFERENCES us (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE us_tag ADD CONSTRAINT FK_3BF9D6ABBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE uw ADD CONSTRAINT FK_DDF07BD325CEDB07 FOREIGN KEY (main_tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE uw_tag ADD CONSTRAINT FK_CE79706B98F81CA5 FOREIGN KEY (uw_id) REFERENCES uw (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE uw_tag ADD CONSTRAINT FK_CE79706BBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE uw_word ADD CONSTRAINT FK_6DEDEF9C98F81CA5 FOREIGN KEY (uw_id) REFERENCES uw (id)');
        $this->addSql('ALTER TABLE uw_word ADD CONSTRAINT FK_6DEDEF9CE357438D FOREIGN KEY (word_id) REFERENCES word (id)');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F1751182F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F1751142C869E2 FOREIGN KEY (paradigm_id) REFERENCES paradigm (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE language DROP FOREIGN KEY FK_D4DB71B5727ACA70');
        $this->addSql('ALTER TABLE paradigm DROP FOREIGN KEY FK_ECF68BE182F1BAF4');
        $this->addSql('ALTER TABLE word DROP FOREIGN KEY FK_C3F1751182F1BAF4');
        $this->addSql('ALTER TABLE word DROP FOREIGN KEY FK_C3F1751142C869E2');
        $this->addSql('ALTER TABLE us_tag DROP FOREIGN KEY FK_3BF9D6ABBAD26311');
        $this->addSql('ALTER TABLE uw DROP FOREIGN KEY FK_DDF07BD325CEDB07');
        $this->addSql('ALTER TABLE uw_tag DROP FOREIGN KEY FK_CE79706BBAD26311');
        $this->addSql('ALTER TABLE us_tag DROP FOREIGN KEY FK_3BF9D6AB179A8BF2');
        $this->addSql('ALTER TABLE uw_tag DROP FOREIGN KEY FK_CE79706B98F81CA5');
        $this->addSql('ALTER TABLE uw_word DROP FOREIGN KEY FK_6DEDEF9C98F81CA5');
        $this->addSql('ALTER TABLE uw_word DROP FOREIGN KEY FK_6DEDEF9CE357438D');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE paradigm');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE us');
        $this->addSql('DROP TABLE us_tag');
        $this->addSql('DROP TABLE uw');
        $this->addSql('DROP TABLE uw_tag');
        $this->addSql('DROP TABLE uw_word');
        $this->addSql('DROP TABLE word');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
