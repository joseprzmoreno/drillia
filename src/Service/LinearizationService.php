<?php

namespace App\Service;

use App\Entity\Word;
use App\Repository\WordRepository;
use App\Entity\Uw;
use App\Repository\UwRepository;
use App\Entity\UwWordRelation;
use App\Repository\UwWordRelationRepository;
use App\Utils\Chunk;
use App\Utils\Linearization;
use App\Utils\UnlNode;
use App\Utils\UnlTree;
use Doctrine\ORM\NonUniqueResultException;
use App\Language\BaseLanguage;
use Doctrine\Persistence\ManagerRegistry;

class LinearizationService
{
    private $uwWordRelationRepository;

    public function __construct(UwWordRelationRepository $uwWordRelationRepository) {
        $this->uwWordRelationRepository = $uwWordRelationRepository;
    }

    public function manageLinearizations(array $tree, array $languageCodes, ManagerRegistry $doctrine): array
    {
        $response = [];
        $unlTree = $this->buildUnlTree($tree);
        $response['unlTree'] = $unlTree;
        $response['unlTreeStr'] = $unlTree->__toString();

        foreach($languageCodes as $lang) {
            $response[$lang] = $this->prelinearize($response['unlTree'], $lang, $doctrine);
        }

        return $response;
    }

    /**
     * @throws NonUniqueResultException
     */
    private function prelinearize(UnlTree $unlTree, string $lang, ManagerRegistry $doctrine): array
    {
        //dividir en oraciones. Para cada una
        //encontrar verbo principal
        //encontrar el sujeto
        //dar forma al verbo
        //dar forma a los elementos requeridos por el verbo
        //dar forma a los otros nodos, añadiendo otros nodos si es necesario
        //linearizar respecto al orden predefinido SVOC, ojo contracciones
        //conectar oraciones, ojo contracciones
        //devolver toda la información y lineralización final con seminodos

        $response = [];
        $languageClassName = "App\\Language\\" . ucfirst($lang);
        $languageClass = new $languageClassName;
        $linearization = $this->createLinearizationFromUnlTree($unlTree, $lang, $doctrine);
        $linearization->buildLinearizationGroups();
        $sublinearizations = [];
        foreach ($linearization->getGroups() as $group)
        {
            $sublinearizations[] = $this->linearize($group, $lang, $languageClass);
        }
        $finalLinearization = Linearization::createNewLinearizationFromGroups($sublinearizations, $linearization);
        $finalLinearization->rearrangePositions();
        $response['linearization'] = $finalLinearization;
        $response['linearizationStr'] = $finalLinearization->__toString();
        $response['plain'] = $finalLinearization->paintPlain();

        return $response;
    }

    private function linearize($linearization, $lang, $languageClass) {
        $linearization = $this->doLexemeSelection($linearization, $lang);
        $linearization = $this->identifyCase($linearization, $languageClass);
        $linearization = $this->morphSynthesis($linearization, $languageClass);
        $linearization = $this->generateFunctionWords($linearization, $languageClass);
        $linearization = $this->linearizeProper($linearization, $languageClass);
        $linearization = $this->connectWithChildLinearization($linearization, $languageClass);
        //$linearization = $this->connectWithParentLinearization($linearization, $languageClass);
        //$liunearization = $this->resolveContractions($linearization, $languageClass);

        return $linearization;

    }

    private function createLinearizationFromUnlTree($unlTree, $lang, ManagerRegistry $doctrine)
    {
        $chunks = [];
        foreach ($unlTree->getNodes() as $node)
        {
            $chunks[] = Chunk::basicLoad($node->getId(), $node->getParent(), $node->getUwLemma(), $node->getAttributes(),
                $node->getFunction(), $node->getChildren(), $node->getColor(), $doctrine);
        }
        return new Linearization($chunks, $unlTree, $lang);
    }

    /**
     * @throws NonUniqueResultException
     */
    private function doLexemeSelection($linearization, $lang)
    {
        foreach ($linearization->getChunks() as &$chunk) {
            if (!in_array($chunk->getUwLemma(), ['*'])) {
                $uwr = $this->uwWordRelationRepository->findHeaviestWordByUwIdentifierAndLanguage(
                    $chunk->getUwLemma(), $lang);
                $chunk->setWordLemma($uwr->getWord()->getLatinLemma());
                $chunk->setWord($uwr->getWord());
            }
        }
        return $linearization;
    }

    private function identifyCase($linearization, $languageClass)
    {
        $caseEquivalences = $languageClass->getCaseEquivalences();

        foreach ($linearization->getChunks() as $chunk) {
            if ($chunk->isEligibleForCase()) {
                if ($linearization->hasParentChunkWithReq($chunk->getId())) {
                    $chunk->setCase($linearization->getChunkCorrespondingCaseForReq($chunk->getId()));
                } else if (array_key_exists($chunk->getFunction(), $caseEquivalences)) {
                    $chunk->setCase($languageClass->getCorrespondingCase($chunk->getFunction()));
                }
            }
        }

        return $linearization;
    }

    private function morphSynthesis($linearization, $languageClass)
    {
        foreach ($linearization->getChunks() as $chunk) {

            if ($chunk->getUwLemma() == '*') {
                $chunk->setForm('*');
            } else {
                $word = $chunk->getWord();
                $params = $chunk->getAttributes();
                if (!empty($chunk->getCase())) {
                    $params['cas'] = $chunk->getCase();
                }
                if ($word->getCat() == 'V') {
                    $this->fetchPersonAndNumberOfVerbSubject($linearization,$chunk, $params);
                }
                if ($word->getCat() == 'PRON' and $chunk->getFunction() == 'agt') {
                    $chunk->setIsVisible(!$languageClass->getIsSubjectPronounDropped());
                }
                $correspondingForm = $word->getCorrespondingForm($params);
                $chunk->setForm($correspondingForm);
            }
        }

        return $linearization;
    }

    private function fetchPersonAndNumberOfVerbSubject($linearization, &$chunk, &$params): void
    {
        $subjectChunk = $chunk->getChunkActingAsSubjectOfVerb($linearization);
        if (!empty($subjectChunk)) {
            if (array_key_exists('per', $subjectChunk->getWord()->getInfoAsArray()) &&
                in_array($subjectChunk->getWord()->getInfoValueByKey('per'), ['p1', 'p2'])) {
                $params['per'] = $subjectChunk->getWord()->getInfoValueByKey('per');
            }
            if (array_key_exists('num', $subjectChunk->getAttributes()) &&
                $subjectChunk->getAttributes()['num'] != 'sg') {
                $params['num'] = $subjectChunk->getAttributes()['num'];
            } else if (array_key_exists('num', $subjectChunk->getWord()->getInfoAsArray()) &&
                $subjectChunk->getWord()->getInfoValueByKey('num') != 'sg') {
                $params['per'] = $subjectChunk->getWord()->getInfoValueByKey('num');
            }
        }
    }

    private function generateFunctionWords($linearization, $languageClass)
    {
        return $languageClass->generateFunctionWords($linearization);
    }

    private function linearizeProper($linearization, $languageClass)
    {
        return $languageClass->linearize($linearization);
    }

    private function connectWithChildLinearization($linearization, $languageClass)
    {
        return $languageClass->connectWithChildLinearization($linearization);
    }

    public function buildUnlTree($tree): UnlTree
    {
        $unlNodes = [];
        foreach ($tree['nodes'] as $node)
        {
            $unlNodes[] = new UnlNode($node['id'], $node['parent'], $node['uw'], $node['attributes'],
                $node['function']);
        }
        return new UnlTree($unlNodes);
    }

}