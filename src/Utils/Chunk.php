<?php

namespace App\Utils;

use App\Entity\Word;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Uw;
use App\Utils\Linearization;

class Chunk
{
    private bool $anim;
    private array $attributes;
    private string $case;
    private string $cat;
    private array $children;
    private array $color;
    private ManagerRegistry $doctrine;
    private string $form;
    private string $function;
    private int $group;
    private int $id;
    private bool $isGroupLead;
    private bool $isVisible;
    private int $parent;
    private int $position;
    private string $rom;
    private string $spacesBefore;
    private string $spacesAfter;
    private Word $word;
    private string $wordLemma;
    private Uw $uw;
    private string $uwLemma;

    function __construct(int $id)
    {
        $this->anim = false;
        $this->attributes = [];
        $this->case = '';
        $this->cat = '';
        $this->children = [];
        $this->color = [];
        $this->form = '?';
        $this->function = '';
        $this->id = $id;
        $this->isVisible = true;
        $this->isGroupLead = false;
        $this->parent = 0;
        $this->position = -1;
        $this->rom = '';
        $this->spacesAfter = ' ';
        $this->spacesBefore = ' ';
        $this->wordLemma = '';
        $this->uwLemma = '';
        $this->group = -1;
    }

    public static function basicLoad(int $id, int $parent, string $uwLemma, array $attributes, string $function,
                                     array $children, array $color, ManagerRegistry $doctrine): self
    {
        $instance = new self($id);
        $instance->setAttributes($attributes);
        $instance->setChildren($children);
        $instance->setColor($color);
        $instance->setDoctrine($doctrine);
        $instance->setFunction($function);
        $instance->setParent($parent);
        $instance->setUwLemma($uwLemma);

        $uwRepository = $doctrine->getManager()->getRepository(Uw::class);

        if ($uwLemma == '*') {
            $instance->setForm('*');
        } else {
            $instance->setUw($uwRepository->findOneByIdentifier($uwLemma));
            $instance->setCat($instance->getUw()->getCat());
        }

        return $instance;

    }

    public static function buildNonUwChunkFromParent(int $id, Chunk $parentChunk, string $form, string $cat): self
    {
        $instance = new self($id);
        $instance->setColor($parentChunk->getColor());
        $instance->setParent($parentChunk->getId());
        $instance->setGroup($parentChunk->getGroup());
        $instance->setCat($cat);
        $instance->setForm($form);

        return $instance;
    }

    public function __toString(): string
    {
        return sprintf("[ %s | %s | %s | %s | %s | %s | %s | %s ]",
            $this->getId(),
            $this->getParent(),
            $this->getWordLemma(),
            $this->getGroup(),
            (empty($this->getCase())) ? '_' : $this->getCase(),
            $this->getForm(),
            $this->isVisible() ? 1 : 0,
            $this->getPosition(),
        );
    }
    
    public function getLatinForm(): string
    {
        return (empty($this->rom)) ? $this->form : $this->rom;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getForm(): string
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm(string $form): void
    {
        $this->form = $form;
    }

    /**
     * @return string|null
     */
    public function getRom(): ?string
    {
        return $this->rom;
    }

    /**
     * @param string $rom
     */
    public function setRom(string $rom): void
    {
        $this->rom = $rom;
    }

    /**
     * @return string
     */
    public function getSpacesBefore(): string
    {
        return $this->spacesBefore;
    }

    /**
     * @param string $spacesBefore
     */
    public function setSpacesBefore(string $spacesBefore): void
    {
        $this->spacesBefore = $spacesBefore;
    }

    /**
     * @return string
     */
    public function getSpacesAfter(): string
    {
        return $this->spacesAfter;
    }

    /**
     * @param string $spacesAfter
     */
    public function setSpacesAfter(string $spacesAfter): void
    {
        $this->spacesAfter = $spacesAfter;
    }

    /**
     * @return int
     */
    public function getParent(): int
    {
        return $this->parent;
    }

    /**
     * @param int $parent
     */
    public function setParent(int $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getUwLemma()
    {
        return $this->uwLemma;
    }

    /**
     * @param string $uwLemma
     */
    public function setUwLemma(string $uwLemma): void
    {
        $this->uwLemma = $uwLemma;
    }

    /**
     * @return Uw
     */
    public function getUw(): Uw
    {
        return $this->uw;
    }

    /**
     * @param Uw $uw
     */
    public function setUw(Uw $uw): void
    {
        $this->uw = $uw;
    }

    /**
     * @return bool
     */
    public function getAnim(): bool
    {
        return $this->anim;
    }

    /**
     * @param bool $anim
     */
    public function setAnim($anim): void
    {
        $this->anim = $anim;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        $keys        = array_keys($this->attributes);
        $defaultKeys = $this->getDefaultKeysByPartOfSpeech($this->getCat());
        $missingKeys = array_values(array_diff($defaultKeys, $keys));
        foreach ($missingKeys as $key) {
            $defaultValue = $this->getDefaultAttribute($key);
            $this->attributes[$key] = $defaultValue;
        }

        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getCase(): string
    {
        return $this->case;
    }

    /**
     * @param string $case
     */
    public function setCase(string $case): void
    {
        $this->case = $case;
    }

    /**
     * @return string
     */
    public function getFunction(): string
    {
        return $this->function;
    }

    /**
     * @param string $function
     */
    public function setFunction(string $function): void
    {
        $this->function = $function;
    }

    /**
     * @return Word
     */
    public function getWord(): Word
    {
        return $this->word;
    }

    /**
     * @param Word $word
     */
    public function setWord(Word $word): void
    {
        $this->word = $word;
    }

    /**
     * @return string
     */
    public function getWordLemma(): string
    {
        return $this->wordLemma;
    }

    /**
     * @param string $wordLemma
     */
    public function setWordLemma(string $wordLemma): void
    {
        $this->wordLemma = $wordLemma;
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    public function addChild($child): void
    {
        $this->children[] = $child;
    }

    /**
     * @return bool
     */
    public function isGroupLead(): bool
    {
        return $this->isGroupLead;
    }

    /**
     * @param bool $isGroupLead
     */
    public function setIsGroupLead(bool $isGroupLead): void
    {
        $this->isGroupLead = $isGroupLead;
    }

    /**
     * @return int
     */
    public function getGroup(): int
    {
        return $this->group;
    }

    /**
     * @param int $group
     */
    public function setGroup(int $group): void
    {
        $this->group = $group;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function isEligibleForCase(): bool
    {
        $isEligible = true;
        if (in_array($this->getUwLemma(), ['*']) || in_array($this->getFunction(), ['v'])) {
            $isEligible = false;
        }
        return $isEligible;
    }

    public function getDefaultKeysByPartOfSpeech($partOfSpeech): array
    {
        return match ($partOfSpeech) {
            'V' => ['tns', 'per', 'num'],
            'N' => ['num'],
            default => [],
        };
    }

    public function getDefaultAttribute($key): string
    {
        return match ($key) {
            'tns' => 'PR',
            'per' => 'p3',
            'num' => 'sg',
            default => '',
        };
    }

    /**
     * @return string
     */
    public function getCat(): string
    {
        return $this->cat;
    }

    /**
     * @param mixed $cat
     */
    public function setCat(string $cat): void
    {
        $this->cat = $cat;
    }

    public function getChunkActingAsSubjectOfVerb(Linearization $linearization): ?Chunk
    {
        foreach ($this->getChildren() as $chunkId) {
            $chunk = $linearization->findChunkById($chunkId);
            if ($chunk->getFunction() === 'agt') {
                return $chunk;
            }
        }
        return null;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     */
    public function setIsVisible(bool $isVisible): void
    {
        $this->isVisible = $isVisible;
    }

    /**
     * @return array
     */
    public function getColor(): array
    {
        return $this->color;
    }

    /**
     * @param array $color
     */
    public function setColor(array $color): void
    {
        $this->color = $color;
    }

    public function attributesHaveParam($key, $value): bool
    {
        $attributes = $this->getAttributes();
        if (!array_key_exists($key, $attributes)) {
            return false;
        }
        return $attributes[$key] == $value;
    }

    public function getAttributeValueByKey($key): ?string
    {
        $attributes = $this->getAttributes();
        if (!array_key_exists($key, $attributes)) {
            return null;
        }
        return $attributes[$key];
    }

    public function addAttributePair($key, $value): void
    {
        $attributes = $this->getAttributes();
        $attributes[$key] = $value;
        $this->setAttributes($attributes);
    }

    /**
     * @return ManagerRegistry
     */
    public function getDoctrine(): ManagerRegistry
    {
        return $this->doctrine;
    }

    /**
     * @param ManagerRegistry $doctrine
     */
    public function setDoctrine(ManagerRegistry $doctrine): void
    {
        $this->doctrine = $doctrine;
    }
}