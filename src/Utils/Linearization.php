<?php

namespace App\Utils;

use App\Utils\Chunk;
use App\Utils\UnlTree;

class Linearization
{
    private array $chunks;
    private array $sortedChunks;
    private ?UnlTree $unlTree;
    private bool $ucInitial;
    private string $lang;
    private string $period;
    private array $groups;
    private int $id;
    private int $parent;

    function __construct(array $chunks, ?UnlTree $unlTree, string $lang, bool $ucInitial = true, string $period = '.')
    {
        $this->chunks = $chunks;
        $this->sortedChunks = [];
        $this->unlTree = $unlTree;
        $this->lang = $lang;
        $this->ucInitial = $ucInitial;
        $this->period = $period;
        $this->groups = [];
        $this->id = 0;
        $this->parent = 0;
    }

    /**
     * @return mixed
     */
    public function getChunks(): array
    {
        return $this->chunks;
    }

    /**
     * @param mixed $chunks
     */
    public function setChunks(array $chunks): void
    {
        $this->chunks = $chunks;
    }

    public function addChunk(Chunk $chunk): void
    {
        $this->chunks[] = $chunk;
    }

    public function __toString(): string
    {
        $linearization = '';
        $chunksArray = [];

        foreach($this->getSortedChunks() as $chunk) {
            $chunksArray[] = $chunk->__toString();
        }

        $chunksTxt = implode("\n", array_merge([''], $chunksArray));
        $linearization .= $chunksTxt;
        return $linearization;
    }

    public function buildLinearizationGroups()
    {
        $asterisks = 0;
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getUwLemma() == '*') {
                $asterisks++;
            }
        }

        if ($asterisks == 0) {
            return;
        }

        $groups = [];
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getParent() == 0 || $chunk->getUwLemma() == '*') {
                $group = new Linearization([], null, $this->lang);
                $group->setId($chunk->getId());
                $chunk->setIsGroupLead(true);
                $groups[] = $group;
            }
        }

        //Assign groups
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getParent() == 0) {
                $chunk->setGroup($chunk->getId());
            } else {
                $chunk->setGroup($this->findClosestGroupLead($chunk->getId()));
            }
        }

        //Fill groups
        foreach ($groups as $group) {
            foreach ($this->getChunks() as $chunk) {
                if ($group->getId() == $chunk->getGroup()) {
                    $group->addChunk($chunk);
                }
            }
        }

        $this->setGroups($groups);
    }

    public function findChunkById(int $chunkId): ?Chunk
    {
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getId() == $chunkId) {
                return $chunk;
            }
        }
        return null;
    }

    public function findClosestGroupLead($chunkId): int
    {
        $parentHasBeenFound = false;
        $parentChunkId = 0;
        $chunk = $this->findChunkById($chunkId);
        do {
            $parentChunk = $this->findChunkById($chunk->getParent());
            if ($parentChunk->isGroupLead()) {
                $parentHasBeenFound = true;
                $parentChunkId = $parentChunk->getId();
            } else {
                $chunk = $parentChunk;
            }
        } while (!$parentHasBeenFound);
        return $parentChunkId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getParent(): int
    {
        return $this->parent;
    }

    /**
     * @param int $parent
     */
    public function setParent(int $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return array
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }

    public function addGroup($group): void
    {
        $this->groups[] = $group;
    }

    public function hasParentChunkWithReq($chunkId): bool
    {
        $chunk = $this->findChunkById($chunkId);
        $parentChunk = $this->findChunkById($chunk->getParent());
        if (array_key_exists('req', (array) $parentChunk->getAttributes())) {
            return true;
        }
        return false;
    }

    public function getChunkCorrespondingCaseForReq($chunkId): ?string
    {
        $chunk = $this->findChunkById($chunkId);
        $function = $chunk->getFunction();
        $parentChunk = $this->findChunkById($chunk->getParent());
        $requirements = $parentChunk->getAttributes()['req'];
        foreach ($requirements as $fun => $case) {
            if ($fun == $function) {
                return $case;
            }
        }
        return null;
    }

    public function getNextIdAvailable(): int
    {
        $ids = [];
        foreach ($this->getChunks() as $chunk) {
            $ids[] = $chunk->getId();
        }
        return max($ids) + 1;
    }

    public function buildChildren(): void
    {
        foreach ($this->getChunks() as $chunk)
        {
            $chunk->setChildren([]);
        }
        foreach ($this->getChunks() as &$chunk)
        {
            foreach ($this->getChunks() as $chunk2) {
                if ($chunk->getId() !== $chunk2->getId()) {
                    if ($chunk2->getParent() === $chunk->getId()) {
                        $chunk->addChild($chunk2->getId());
                    }
                }
            }
        }
    }

    public function findMinimumId(): int
    {
        $ids = [];
        foreach ($this->getChunks() as $chunk) {
            $ids[] = $chunk->getId();
        }
        return min($ids);
    }

    public function getSortedChunks(): array
    {
        $this->buildChildren();
        $hasChunkWithParentIdZero = false;
        $initialId = 0;
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getParent() == 0) {
                $hasChunkWithParentIdZero = true;
                $initialId = $chunk->getId();
            }
        }
        if (!$hasChunkWithParentIdZero) {
            $minimumId = $this->findMinimumId();
            foreach ($this->getChunks() as $chunk) {
                if ($chunk->getParent() < $minimumId) {
                    $initialId = $chunk->getId();
                }
            }
        }
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getId() === $initialId) {
                $this->walk($chunk);
            }
        }
        return $this->sortedChunks;
    }

    public function walk($chunk): void
    {
        $this->sortedChunks[] = $chunk;
        foreach ($chunk->getChildren() as $child) {
            $this->walk($this->findChunkById($child));
        }
    }

    public static function createNewLinearizationFromGroups(array $groups, Linearization $originalLinearization): self
    {
        $chunks = [];
        foreach ($groups as $group) {
            foreach ($group->getChunks() as $chunk) {
                $chunks[] = $chunk;
            }
        }
        return new self(
            $chunks,
            $originalLinearization->getUnlTree(),
            $originalLinearization->getLang(),
            $originalLinearization->isUcInitial(),
            $originalLinearization->getPeriod()
        );
    }

    /**
     * @return \App\Utils\UnlTree
     */
    public function getUnlTree(): \App\Utils\UnlTree
    {
        return $this->unlTree;
    }

    /**
     * @param \App\Utils\UnlTree $unlTree
     */
    public function setUnlTree(\App\Utils\UnlTree $unlTree): void
    {
        $this->unlTree = $unlTree;
    }

    /**
     * @return bool
     */
    public function isUcInitial(): bool
    {
        return $this->ucInitial;
    }

    /**
     * @param bool $ucInitial
     */
    public function setUcInitial(bool $ucInitial): void
    {
        $this->ucInitial = $ucInitial;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang): void
    {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getPeriod(): string
    {
        return $this->period;
    }

    /**
     * @param string $period
     */
    public function setPeriod(string $period): void
    {
        $this->period = $period;
    }

    public function findHighestPosition(): int
    {
        $positions = [-1];
        foreach ($this->getChunks() as $chunk) {
            $positions[] = (empty($chunk->getPosition())) ? -1 : $chunk->getPosition();
        }
        return max($positions);
    }

    public function findHighestPositionBeforeX(int $top): int
    {
        $positions = [0];
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getPosition() < $top) {
                $positions[] = $chunk->getPosition();
            }
        }
        return max($positions);
    }

    public function getNewPositionAtTheEnd(): int
    {
        return $this->findHighestPosition() + 1000;
    }

    public function getPositionImmediatelyBeforeParent(int $parentChunkId): int
    {
        $parentChunk = $this->findChunkById($parentChunkId);
        $parentPosition = $parentChunk->getPosition();
        $bottom = $this->findHighestPositionBeforeX($parentPosition);
        return ($bottom == 0) ? floor($parentPosition / 2) : floor(($parentPosition - $bottom) / 2);
    }

    public function getDistinctGroupIds(): array
    {
        $ids = [];
        foreach ($this->getChunks() as $chunk) {
            $ids[] = $chunk->getGroup();
        }
        $uniqueIds = array_unique($ids);
        sort($uniqueIds);
        return $uniqueIds;
    }

    public function rearrangePositions(): void
    {
        $groups = $this->getDistinctGroupIds();

        foreach ($groups as $group) {

            foreach ($this->getChunks() as $chunk) {
                if ($chunk->getGroup() === $group and $group !== min($groups)) {
                    $parentGroupMaxPosition = $this->findMaximumPositionOfChunkInsideGroup(
                        $this->findParentGroupId($group));
                    //TODO buscar signo para saber si se pone antes o después
                    //TODO chequear si en los grupos padres tienen que cambiar también de posición
                    $chunk->setPosition($parentGroupMaxPosition + 1000 + $chunk->getPosition());
                }
            }
        }
    }

    public function findParentGroupId(int $childGroupId): int
    {
        $minimumChunkId = $this->findMinimumChunkIdInsideGroup($childGroupId);
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getGroup() === $childGroupId and $chunk->getParent() < $minimumChunkId) {
                return $this->findChunkById($chunk->getParent())->getGroup();
            }
        }
        return -1;
    }

    public function findMinimumChunkIdInsideGroup($groupId): int
    {
       $ids = [];
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getGroup() === $groupId) {
                $ids[] = $chunk->getId();
            }
        }
        return min($ids);
    }

    public function findMaximumPositionOfChunkInsideGroup(int $groupId): int
    {
        $positions = [];
        foreach ($this->getChunks() as $chunk) {
            if ($chunk->getGroup() === $groupId) {
                $positions[] = $chunk->getPosition();
            }
        }
        return max($positions);
    }


    public function getChunksSortedByPosition(): array
    {
        $chunks = $this->getChunks();
        usort($chunks, fn($a, $b) => $a->getPosition() > $b->getPosition());
        return $chunks;
    }

    public function paintPlain(): string
    {
        $sortedChunks = $this->getChunksSortedByPosition();
        $res = '';
        foreach ($sortedChunks as $chunk) {
            if ($chunk->isVisible()) {
                $res .= $chunk->getSpacesBefore() . $chunk->getForm() . $chunk->getSpacesAfter();
            }
        }
        $res = trim($res);
        $res = preg_replace('/\s+/', ' ', $res);
        $res .= $this->getPeriod();
        if ($this->isUcInitial()) {
            $res = ucfirst($res);
        }
        return $res;
    }
}