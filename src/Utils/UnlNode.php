<?php

namespace App\Utils;

use App\Repository\UwRepository;

class UnlNode
{
    private $id;
    private $parent;
    private $uwLemma;
    private array $attributes;
    private $function;
    private array $children;
    private array $color;

    function __construct($id, $parent, $uwLemma, array $attributes, $function)
    {
        $this->id = $id;
        $this->parent = $parent;
        $this->uwLemma = $uwLemma;
        $this->attributes = $attributes;
        $this->function = $function;
        $this->children = [];
        $this->color = $this->getRandomColor();
    }

    /**
     * @return mixed
     */
    public function getUwLemma()
    {
        return $this->uwLemma;
    }

    /**
     * @param mixed $uwLemma
     */
    public function setUwLemma($uwLemma): void
    {
        $this->uwLemma = $uwLemma;
    }

    public function __toString(): string
    {
        return sprintf("[ %s | %s | %s | %s | %s | %s ]", $this->getId(), $this->getParent(), $this->getUwLemma(),
        json_encode($this->getAttributes()), $this->getFunction(), json_encode($this->getChildren()));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param mixed $function
     */
    public function setFunction($function): void
    {
        $this->function = $function;
    }

    public function addChild($child): void
    {
        $this->children[] = $child;
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    private function getRandomColor(): array
    {
        $min = 127;
        $max = 235;
        $r = rand($min, $max);
        $g = rand($min, $max);
        $b = rand($min, $max);

        return [$r, $g, $b];
    }

    /**
     * @return array
     */
    public function getColor(): array
    {
        return $this->color;
    }

    /**
     * @param array $color
     */
    public function setColor(array $color): void
    {
        $this->color = $color;
    }
}