<?php

namespace App\Utils;

class ToolRepository
{
    public static function isValidJson($string): bool
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}