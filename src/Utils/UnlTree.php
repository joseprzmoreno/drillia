<?php

namespace App\Utils;

class UnlTree
{
    private $nodes;
    /**
     * @var array|mixed
     */
    private mixed $extra;
    private array $sortedNodes;

    function __construct($nodes, $extra = [])
    {
        $this->nodes = $nodes;
        $this->extra = $extra;
        $this->sortedNodes = [];
        $this->sortByDepth();
    }

    function __toString(): string
    {
        $chunks = [];
        foreach ($this->getSortedNodes() as $node) {
            $chunks[] = $node->__toString();
        }
        return "\n".implode("\n", $chunks);
    }

    public function getNodes()
    {
        return $this->nodes;
    }

    public function getSortedNodes(): array
    {
       return $this->sortedNodes;
    }

    public function addNode($node): void
    {
        $this->nodes[] = $node;
    }

    public function deductNodesOrderForLinearization()
    {

    }

    public function isSimpleSentence(): bool
    {
        $asterisks = 0;
        foreach ($this->getNodes() as $node) {
            if ($node->getUwLemma() == '*') {
                $asterisks++;
            }
        }
        return $asterisks == 0;
    }

    public function buildChildren()
    {
        foreach ($this->getNodes() as &$node)
        {
            foreach ($this->getNodes() as $node2) {
                if ($node->getId() !== $node2->getId()) {
                    if ($node2->getParent() === $node->getId()) {
                        $node->addChild($node2->getId());
                    }
                }
            }
        }
    }

    public function sortByDepth(): void
    {
        $this->buildChildren();
        foreach ($this->getNodes() as $node) {
            if ($node->getParent() == 0) {
                $this->walk($node);
            }
        }
    }

    public function walk($node): void
    {
        $this->sortedNodes[] = $node;
        foreach ($node->getChildren() as $child) {
            $this->walk($this->findNodeById($child));
        }
    }

    public function findNodeById($nodeId)
    {
        foreach ($this->nodes as $node) {
            if ($node->getId() == $nodeId) {
                return $node;
            }
        }
    }
}