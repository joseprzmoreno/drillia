<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

use App\Service\LinearizationService;

class LinearizationController extends AbstractController
{
    /**
     * @Route("/api/linearize/{langs}/{argsString}", name="app_linearization", requirements={"argsString"=".+"})
     */
    public function index($langs, $argsString, LinearizationService $linearizer, ManagerRegistry $doctrine): JsonResponse
    {
        $languageCodes = $this->parseLanguages($langs);
        $isValid = $this->isValidLinearizationRoute($argsString);
        $response = ['errorMessage' => ''];

        if (!$isValid) {
            $response['errorMessage'] = 'The string is not valid for linearization';
            return new JsonResponse($response);
        }     

        $tree = $this->parseArgs($argsString);
        $response['data'] = $linearizer->manageLinearizations($tree, $languageCodes, $doctrine);

        $jsonResponse = new JsonResponse($response);
        return $jsonResponse;
    }

    private function isValidLinearizationRoute(string $argsString): bool
    {
        $isValid = true;
        $chunks = explode('/', $argsString);
        if (count($chunks) < 5 || count($chunks) % 5 > 0) {
            $isValid = false;
        }       
        return $isValid;
    }

    private function parseLanguages(string $langCodes): array
    {
        return explode('-', $langCodes);
    }

    private function parseArgs(string $argsString): array
    {
        $tree = [];
        if (str_ends_with($argsString, '/')) {
            $argsString = substr($argsString, 0, strlen($argsString)-1);
        }
        $chunks = explode('/', $argsString);
        for ($i=0; $i<count($chunks); $i=$i+5) {
            $node = [
                'id' => $chunks[$i],
                'parent' => $chunks[$i+1],
                'uw' => $chunks[$i+2],
                'attributes' => $this->jsonize($chunks[$i+3]),
                'function' => $chunks[$i+4],
            ];
            $tree['nodes'][] = $node;
        }
        return $tree;
    }

    private function jsonize($string): array
    {
        if ($string !== '_') {
            $chunks = explode(',', $string);
            $newChunks = [];
            foreach ($chunks as $chunk) {
                $chunk = trim($chunk);
                $bits = explode(':', $chunk);
                $left = '"' . $bits[0] . '"';
                $right = '"' . $bits[1] . '"';
                $newChunks[] = $left . ':' . $right;
            }
            return json_decode('{' . implode(',', $newChunks) . '}', true);
        }
        return [];
    }
}
