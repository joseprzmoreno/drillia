<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Component\Uid\Uuid;

use App\Entity\Language;
use App\Repository\LanguageRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class RegistrationController extends AbstractController
{
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $request->setLocale($locale);
    }

    #[Route('{_locale}/register', name: 'app_register')]
    public function register(LanguageRepository $languageRepository): Response
    {

        if ($this->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException('Log out to access this page');
        }

        $languages = $languageRepository->findAllActiveLanguagesSorted();

        return $this->render('register.html.twig', [
            'languages' => $languages,
        ]);
    }

    #[Route('/register_action', name: 'register_action', methods: ['POST'])]
    public function registerAction(
        Request $request, 
        UserPasswordHasherInterface $userPasswordHasher, 
        EntityManagerInterface $entityManager,
        LanguageRepository $languageRepository,
        TranslatorInterface $translator,
        MailerInterface $mailer): 
            Response
    {
        $user = new User();
        $user->setUuid(Uuid::v4());
        $user->setUserActivated(false);
        $now = date('Y-m-d H:i:s'); 
        $user->setCreationDate(\DateTime::createFromFormat('Y-m-d H:i:s', $now));
        $token = $this->generateRandomToken();

        $user->setUsername($request->request->get('username'));
        $user->setEmail($request->request->get('userEmail'));
        $encryptedPassword = $userPasswordHasher->hashPassword($user,$request->request->get('userPassword'));
        $user->setPassword($encryptedPassword);
        $language = $languageRepository->findOneBy(['code' => $request->request->get('motherTongue')]);
        $user->setMotherTongue($language);
        $user->setAgreeTerms(true);
        $user->setIsVip(false);
        $user->setPasswordRequested(false);
        $user->setToken($token);
        $user->setRoles(['ROLE_USER']);

        $entityManager->persist($user);
        $entityManager->flush();

        $emailSubject = $translator->trans('activation_mail_subject');
        $email = (new TemplatedEmail())
            ->from('mail@drillia.org')
            ->to($user->getEmail())
            ->subject($emailSubject)
            ->htmlTemplate('mails/mail_activate_profile.html.twig')
            ->context(['activationLink' => 'https://drillia.org/' . $request->getLocale() . '/' . $token . '/activate_profile/' . $user->getUuid()]);
        $mailer->send($email);

        return $this->render('register_final.html.twig', [
            'register_completed' => $translator->trans('register_completed'),
        ]);
    }

    #[Route('/{_locale}/{token}/activate_profile/{userUuid}', name: 'activate_profile', methods: ['GET'])]
    public function activateProfile(Request $request, 
        string $token, 
        string $userUuid,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        MailerInterface $mailer): Response
    {
        $message = $translator->trans('profile_activated');
        $user = $userRepository->findOneBy(['uuid' => $userUuid]);
        if (isset($user)) {
            if ($user->getToken() === $token) {
                if ($user->getUserActivated()) {
                    $message = $translator->trans('profile_activated_error_user_already_activated');
                } else {
                    $user->setUserActivated(true);
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
            } else {
                $message = $translator->trans('profile_activated_error_token_not_correct');
            }
        } else {
            $message = $translator->trans('profile_activated_error_no_user');
        }

        $emailSubject = $translator->trans('profile_activated_mail_subject');
        $email = (new TemplatedEmail())
            ->from('mail@drillia.org')
            ->to($user->getEmail())
            ->subject($emailSubject)
            ->htmlTemplate('mails/mail_profile_activated.html.twig');
        $mailer->send($email);
        
        return $this->render('register_completed.html.twig', [
            'message' => $message,
        ]); 
    }


    #[Route('/{_locale}/check_username_and_email', name: 'check_username_and_email', methods: ['POST'])]
    public function checkUsernameAndEmail(Request $request, 
        EntityManagerInterface $entityManager,
        UserRepository $userRepository): JsonResponse
    {
        $user = $userRepository->findOneBy(['username' => $request->request->get('username')]);
        if (!empty($user)) {
            return new JsonResponse(['ans' => '1']);
        }
        $user = $userRepository->findOneBy(['email' => $request->request->get('mail')]);
        if (!empty($user)) {
            return new JsonResponse(['ans' => '2']);
        }
        return new JsonResponse(['ans' => '0']);
        
    }

    private function generateRandomToken()
    {
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $token = '';
        for ($i=0; $i<=64; $i++) {
            $z = rand(0,strlen($str)-1);
            $token .= substr($str, $z, 1);
        }
        return $token;
    }
}
