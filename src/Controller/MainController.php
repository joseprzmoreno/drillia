<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Language;
use App\Repository\LanguageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MainController extends AbstractController
{

    #[Route('/{_locale}/prehome', name: 'app_prehome')]
    public function prehome(): Response
    {
        return $this->render('prehome.html.twig', [
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('{_locale}/home', name: 'app_home')]
    public function home(): Response
    {
        return $this->render('home.html.twig', [
        ]);
    }

    /*#[Route('/register', name: 'app_register')]
    public function register(LanguageRepository $languageRepository): Response
    {
        $languages = $languageRepository->findAll();

        return $this->render('register.html.twig', [
            'languages' => $languages,
        ]);
    }*/
}
