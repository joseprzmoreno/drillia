<?php

namespace App\Controller\Admin;

use App\Entity\Uw;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class UwCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Uw::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('lemma'),
            TextField::new('shortDefinition'),
            TextField::new('identifier'),
            IntegerField::new('ordering'),
            TextField::new('cat'),
            TextareaField::new('info'),
            BooleanField::new('isBasic'),
            TextField::new('properNoun'),
            BooleanField::new('active'),
            TextareaField::new('extra'),
            AssociationField::new('mainTag'),
            AssociationField::new('tags'),
            BooleanField::new('isInGenerator'),
        ];
    }
    
}
