<?php

namespace App\Controller\Admin;

use App\Entity\UwWordRelation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class UwWordRelationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UwWordRelation::class;
    }

    public function configureFields(string $pageName): iterable
   {
        return [
                IdField::new('id')->hideOnForm(),
                AssociationField::new('uw'),
                AssociationField::new('word'),
                IntegerField::new('weight'),
        ];
    }

}