<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityController extends AbstractController
{
    #[Route(path: '{_locale}/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    { 
        if ($this->getUser()) {
             return $this->redirectToRoute('app_home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '{_locale}/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route(path: '{_locale}/recover_password', name: 'recover_password')]
    public function recoverPassword(
        Request $request, 
        UserRepository $repository,
        MailerInterface $mailer,
        TranslatorInterface $translator,
        EntityManagerInterface $entityManager
    ): JsonResponse
    {
        $data['ans'] = '0';

        $user = $repository->findOneBy(['email' => $request->request->get('email')]);

        if (empty($user) || !$user->getUserActivated()) {
            $data['ans'] = '1';
            return new JsonResponse($data);
        }

        if ($user->getPasswordRequested()) {
            $data['ans'] = '2';
            return new JsonResponse($data);
        }

        $user->setPasswordRequested(true);        
        $entityManager->persist($user);
        $entityManager->flush();

        $emailSubject = $translator->trans('mail_recover_password_subject');
        $email = (new TemplatedEmail())
            ->from('mail@drillia.org')
            ->to($user->getEmail())
            ->subject($emailSubject)
            ->htmlTemplate('mails/mail_recover_password.html.twig')
            ->context([
                'message' => $translator->trans('mail_recover_password_text'),
                'recoveryLink' => 'https://drillia.org/' . $request->getLocale() . '/' . $user->getToken() . '/recover_password/' . $user->getUuid()
            ])
        ;
        $mailer->send($email);

        return new JsonResponse($data);
    }

    #[Route('/{_locale}/{token}/recover_password/{userUuid}', name: 'recover_password_action', methods: ['GET'])]
    public function recoverPasswordAction(Request $request, 
        string $token, 
        string $userUuid,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        UserRepository $userRepository)
    {
        $okMessage = $translator->trans('message_show_password_screen_ok');
        $koMessage = $translator->trans('message_show_password_screen_ko');
        $user = $userRepository->findOneBy(['uuid' => $userUuid]);
        $showPasswordChangeScreen = true;
        if (!empty($user)) {
            if ($user->getToken() === $token) {
                if (!$user->getUserActivated()) {
                   $showPasswordChangeScreen = false;
                }
            } else {
                $showPasswordChangeScreen = false;
            }
        } else {
            $showPasswordChangeScreen = false;
        }
        
        if ($showPasswordChangeScreen) {
            return $this->render('password_change_screen_ok.html.twig', [
                'message' => $okMessage,
                'email' => $user->getEmail(),
                'userUuid' => $user->getUuid(),
                'token' => $user->getToken(),
            ]); 
        }
        return $this->render('password_change_screen_ko.html.twig', [
            'message' => $koMessage,
        ]); 
    }

    #[Route('/{_locale}/changePassword', name: 'change_password', methods: ['POST'])]
    public function changePassword(Request $request, 
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        MailerInterface $mailer,
        UserPasswordHasherInterface $hasher,
        ): Response
    {
        $token = $request->request->get('token');
        $userUuid = $request->request->get('userUuid');
        $newPassword = $request->request->get('userPassword');
        $okMessage = $translator->trans('message_change_password_ok');
        $koMessage = $translator->trans('message_change_password_ko');
        $user = $userRepository->findOneBy(['uuid' => $userUuid]);
        $passwordChanged = true;
        if (!empty($user)) {
            if ($user->getToken() === $token) {
                if (!$user->getUserActivated()) {
                   $passwordChanged = false;
                }
            } else {
                $passwordChanged = false;
            }
        } else {
            $passwordChanged = false;
        }

        if(!$passwordChanged) {
            return $this->render('password_change_result_ko.html.twig', [
                'message' => $koMessage,
            ]); 
        }

        $encryptedPassword = $hasher->hashPassword($user,$newPassword);
        $user->setPassword($encryptedPassword);
        $user->setPasswordRequested(false);        
        $entityManager->persist($user);
        $entityManager->flush();

        $emailSubject = $translator->trans('mail_password_changed_subject');
        $email = (new TemplatedEmail())
            ->from('mail@drillia.org')
            ->to($user->getEmail())
            ->subject($emailSubject)
            ->htmlTemplate('mails/mail_password_changed.html.twig')
            ->context([
                'message' => $okMessage,
                'loginUrl' => sprintf('https://drillia.org/%s/login', $request->getLocale()),
            ]);
        $mailer->send($email);

        return $this->render('password_change_result_ok.html.twig', [
            'message' => $okMessage,
            'email' => $user->getEmail(),
        ]); 
        
    }
}
