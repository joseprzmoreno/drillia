<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Language;
use App\Repository\LanguageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[IsGranted('ROLE_USER')]
class ProfileController extends AbstractController
{

    #[Route('/{_locale}/profile', name: 'app_profile')]
    public function index(
        LanguageRepository $languageRepository,
    ): Response
    {
        $languages = $languageRepository->findAllActiveLanguagesSorted();

        return $this->render('profile/index.html.twig', [
            'languages' => $languages,
        ]);
    }

    #[Route('/save_profile', name: 'app_save_profile')]
    public function saveProfile(
        Request $request,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        LanguageRepository $languageRepository,
        UserPasswordHasherInterface $hasher
    ): Response
    {
        $ans['ans'] = '1';
        $uuid = $request->request->get('uuid');
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $motherTongue = $request->request->get('motherTongue');
        $name = $request->request->get('name');
        $surname = $request->request->get('surname');
        $company = $request->request->get('company');
        $identificationNumber = $request->request->get('idNumber');
        $address = $request->request->get('address');
        $zipCode = $request->request->get('zipCode');
        $city = $request->request->get('city');
        $region = $request->request->get('region');
        $country = $request->request->get('country');

        $user = $userRepository->findOneBy(['uuid' => $uuid]);

        if (empty($user)){
            $ans['ans'] = '0';
            return new JsonResponse($ans);
        }

        try {

            if (!empty($password)){
                $encryptedPassword = $hasher->hashPassword($user,$password);
                $user->setPassword($encryptedPassword);
            }

            $language = $languageRepository->findOneBy(['code' => $motherTongue]);
            $user->setMotherTongue($language);

            if (!empty($username)) $user->setUsername($username);
            if (!empty($identificationNumber)) $user->setIdentificationNumber($identificationNumber);
            $user->setCompany($company);
            if (!empty($name)) $user->setName($name);
            if (!empty($surname)) $user->setSurname($surname);
            if (!empty($address)) $user->setAddress($address);
            if (!empty($zipCode)) $user->setZipCode($zipCode);
            if (!empty($city)) $user->setCity($city);
            if (!empty($region)) $user->setRegion($region);
            if (!empty($country)) $user->setCountry($country);

            $entityManager->persist($user);
            $entityManager->flush();

            return new JsonResponse($ans);

        } catch (Exception $ex) {
            $ans['ans'] = '0';
            $ans['message'] = $ex->getMessage();
            return new JsonResponse($ans);
        }
    }

    #[Route('/payment_info_is_complete', name: 'app_payment_info_is_complete')]
    public function paymentInfoIsComplete(Request $request, UserRepository $userRepository,
    ): JsonResponse
    {
        $uuid = $request->request->get('uuid');
        $user = $userRepository->findOneBy(['uuid' => $uuid]);
        return new JsonResponse(['ans' => $user->paymentInfoIsComplete()]);
    }
}
