<?php

namespace App\Repository;

use App\Entity\Uw;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Uw|null find($id, $lockMode = null, $lockVersion = null)
 * @method Uw|null findOneBy(array $criteria, array $orderBy = null)
 * @method Uw[]    findAll()
 * @method Uw[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UwRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Uw::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Uw $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Uw $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Uw[] Returns an array of Uw objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOneByIdentifier(string $lemma): ?Uw
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.identifier = :val')
            ->setParameter('val', $lemma)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
