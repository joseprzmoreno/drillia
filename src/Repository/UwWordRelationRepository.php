<?php

namespace App\Repository;

use App\Entity\UwWordRelation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\UwRepository;
use App\Entity\Uw;
use App\Entity\Word;
use App\Entity\Language;

/**
 * @method UwWordRelation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UwWordRelation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UwWordRelation[]    findAll()
 * @method UwWordRelation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UwWordRelationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UwWordRelation::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(UwWordRelation $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(UwWordRelation $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findHeaviestWordByUwIdentifierAndLanguage(string $uwIdentifier, string $langCode): ?UwWordRelation
    {
        return $this->createQueryBuilder('u')
            ->join('u.uw','uw')
            ->join('u.word', 'w')
            ->join('w.language', 'l')
            ->andWhere('uw.identifier = :val')
            ->andWhere('l.code = :lang')
            ->setParameter('val', $uwIdentifier)
            ->setParameter('lang', $langCode)
            ->orderBy('u.weight', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return UwWordRelation[] Returns an array of UwWordRelation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UwWordRelation
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
