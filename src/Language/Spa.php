<?php

namespace App\Language;

use App\Utils\Chunk;
use App\Utils\Linearization;
use PhpParser\Node\Scalar\MagicConst\Line;

class Spa extends BaseLanguage
{

    public function __construct()
    {
        $this->setCaseEquivalences([
            'agt' => 'nom',
            'obj' => 'acc',
            'ben' => 'dat',
        ]);
    }

    public function linearize(Linearization $linearization)
    {
        foreach ($linearization->getSortedChunks() as $chunk) {
            if ($chunk->getFunction() === 'v') {
                $chunk->setPosition(1000);
            }
        }

        foreach ($linearization->getSortedChunks() as $chunk) {
            if ($chunk->getFunction() === 'agt') {
                $chunk->setPosition(500);
            }
        }

        foreach ($linearization->getSortedChunks() as $chunk) {
            if ($chunk->getFunction() === 'obj') {
                $chunk->setPosition($linearization->getNewPositionAtTheEnd());
            }
            else if ($chunk->getCat() === 'DET') {
                $chunk->setPosition($linearization->getPositionImmediatelyBeforeParent($chunk->getParent()));
            }
        }

        return $linearization;
    }

    public function getIsSubjectPronounDropped(): bool
    {
        return true;
    }

    public function generateFunctionWords($linearization): Linearization
    {
        foreach ($linearization->getChunks() as $chunk)
        {
            if ($chunk->getForm() !== '*') {
                $this->checkForDefiniteArticle($linearization, $chunk);
                $this->checkForIndefiniteArticle($linearization, $chunk);
                $this->checkForDirectObjectPrepositionalMarker($linearization, $chunk);
            }
        }
        return $linearization;
    }

    private function checkForDefiniteArticle(Linearization &$linearization, Chunk &$chunk)
    {
        if ($chunk->getWord()->getCat() == 'N' and $chunk->attributesHaveParam('def', 1)) {
            $linearization->addChunk($this->createDefiniteArticleChunk(
                $chunk->getWord()->getInfoValueByKey('gen'),
                $chunk->getAttributeValueByKey('num'),
                $linearization->getNextIdAvailable(),
                $linearization->findChunkById($chunk->getId()),
            ));
        }
    }

    private function checkForIndefiniteArticle(Linearization &$linearization, Chunk &$chunk)
    {
        if ($chunk->getWord()->getCat() == 'N' and $chunk->attributesHaveParam('num', 'sg') and
            !$chunk->attributesHaveParam('def', 1)) {
            $linearization->addChunk($this->createSingularIndefiniteArticleChunk(
                $chunk->getWord()->getInfoValueByKey('gen'),
                $linearization->getNextIdAvailable(),
                $linearization->findChunkById($chunk->getId()),
            ));
        }
    }

    private function checkForDirectObjectPrepositionalMarker(Linearization &$linearization, Chunk &$chunk)
    {
        if ($chunk->getCase() === 'Acc' and $chunk->getUw()->getAnim()) {
            $linearization->addChunk($this->createPrepositionalMarkerForDirectObjectChunk(
                $linearization->getNextIdAvailable(), $linearization->findChunkById($chunk->getId()),
            ));
        }
    }

    private function createDefiniteArticleChunk(string $gender, string $number, int $id, Chunk $parentChunk): Chunk
    {
        $articleForms = ['m' => ['sg' => 'el', 'pl' => 'los'], 'f' => ['sg' => 'la', 'pl' => 'las']];
        $form = $articleForms[$gender][$number];
        return Chunk::buildNonUwChunkFromParent($id, $parentChunk, $form, 'DET');
    }

    private function createSingularIndefiniteArticleChunk(string $gender, int $id, Chunk $parentChunk): Chunk
    {
        $articleForms = ['m' => 'un', 'f' => 'una'];
        $form = $articleForms[$gender];
        return Chunk::buildNonUwChunkFromParent($id, $parentChunk, $form, 'DET');
    }

    private function createPrepositionalMarkerForDirectObjectChunk(int $id, Chunk $parentChunk): Chunk
    {
        $chunk = Chunk::buildNonUwChunkFromParent($id, $parentChunk, 'a', 'PREP');
        $chunk->addAttributePair('DIRECT_OBJECT_MARKER', 1);
        return $chunk;
    }

    public function connectWithChildLinearization(Linearization $linearization): Linearization
    {
        foreach ($linearization->getChunks() as $chunk) {
            if ($chunk->getForm() == '*') {
                if ($chunk->getFunction() === 'obj') {
                    $chunk->setForm('que');
                }
            }
        }

        return $linearization;
    }
}