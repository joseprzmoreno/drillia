<?php

namespace App\Language;

class BaseLanguage
{
    /**
     * @var string[]
     */
    private array $caseEquivalences;

    /**
     * @return string[]
     */
    public function getCaseEquivalences(): array
    {
        return $this->caseEquivalences;
    }

    public function getCorrespondingCase(string $unlFunctionName): ?string
    {
        foreach ($this->getCaseEquivalences() as $unlFun => $case) {
            if ($unlFun === $unlFunctionName) {
                return $case;
            }
        }
        return null;
    }

    /**
     * @param string[] $caseEquivalences
     */
    public function setCaseEquivalences(array $caseEquivalences): void
    {
        $this->caseEquivalences = $caseEquivalences;
    }

}