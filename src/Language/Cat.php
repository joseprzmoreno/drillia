<?php

namespace App\Language;

class Cat extends BaseLanguage
{
    public function __construct()
    {
        $this->setCaseEquivalences([
            'agt' => 'nom',
            'obj' => 'acc',
            'ben' => 'dat',
        ]);
    }

    public function getIsSubjectPronounDropped(): bool
    {
        return true;
    }
}