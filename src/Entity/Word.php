<?php

namespace App\Entity;

use App\Repository\WordRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Utils\ToolRepository;
use Exception;

#[ORM\Entity(repositoryClass: WordRepository::class)]
class Word
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Language::class, inversedBy: 'words')]
    #[ORM\JoinColumn(nullable: false)]
    private $language;

    #[ORM\Column(type: 'string', length: 100)]
    private $lemma;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $origLemma;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $variants;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $ipa;

    #[ORM\Column(type: 'string', length: 20)]
    private $cat;

    #[ORM\Column(type: 'text', nullable: true)]
    private $info;

    #[ORM\ManyToOne(targetEntity: Paradigm::class, inversedBy: 'words')]
    private $paradigm;

    #[ORM\Column(type: 'text', nullable: true)]
    private $forms;

    #[ORM\Column(type: 'boolean')]
    private $active;

    #[ORM\Column(type: 'text', nullable: true)]
    private $extra;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getLemma(): ?string
    {
        return $this->lemma;
    }

    public function setLemma(string $lemma): self
    {
        $this->lemma = $lemma;

        return $this;
    }

    public function getOrigLemma(): ?string
    {
        return $this->origLemma;
    }

    public function setOrigLemma(?string $origLemma): self
    {
        $this->origLemma = $origLemma;

        return $this;
    }

    public function getVariants(): ?string
    {
        return $this->variants;
    }

    public function setVariants(?string $variants): self
    {
        $this->variants = $variants;

        return $this;
    }

    public function getIpa(): ?string
    {
        return $this->ipa;
    }

    public function setIpa(?string $ipa): self
    {
        $this->ipa = $ipa;

        return $this;
    }

    public function getCat(): ?string
    {
        return $this->cat;
    }

    public function setCat(string $cat): self
    {
        $this->cat = $cat;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function getInfoAsArray(): ?array
    {
        if (empty($this->info)) {
            return [];
        }
        return json_decode($this->info, true);
    }

    public function getInfoValueByKey(string $key): ?string
    {
        return json_decode($this->info, true)[$key];
    }

    public function setInfo(?string $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function getParadigm(): ?Paradigm
    {
        return $this->paradigm;
    }

    public function setParadigm(?Paradigm $paradigm): self
    {
        $this->paradigm = $paradigm;

        return $this;
    }

    public function getForms(): ?string
    {
        return $this->forms;
    }

    public function setForms(?string $forms): self
    {
        $this->forms = $forms;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getExtra(): ?string
    {
        return $this->extra;
    }

    public function setExtra(?string $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getLatinLemma(): ?string
    {
        return (empty ($this->getLemma())) ? $this->getOrigLemma() : $this->getLemma();
    }

    public function __toString(): string
    {
        $lemma = (empty ($this->getOrigLemma())) ? $this->getLemma() : $this->getOrigLemma();
        return $this->getLanguage()->getCode() . '::' . $lemma;
    }

    /**
     * @throws Exception
     */
    public function buildForms(): string|array
    {
        if (empty($this->getForms()) and empty($this->getParadigm())) {
            return $this->getLemma();
        }
        $paradigmTxt = (empty($this->getForms()) ? $this->getParadigm()->getForms() : $this->getForms());
        if (!ToolRepository::isValidJson($paradigmTxt)) {
            throw new Exception(sprintf('Forms of %s are being generated from a wrong paradigm',
                $this->getLemma()));
        }
        $paradigm = json_decode($paradigmTxt, true);
        $specs = $paradigm['ss'];
        $forms = $paradigm['fs'];
        $orderOfKeys = $paradigm['ks'];
        $formsTxt = json_encode($forms);
        $mainForm = $this->getLemma();
        if (array_key_exists('_', $specs)) {
            $cutout = intval($specs['_']);
            $mainForm = substr($mainForm, 0, strlen($mainForm) + $cutout);
        }
        if (empty($specs)) {
            $formsTxt = str_replace('_', $mainForm, $formsTxt);
        } else {
            foreach ($specs as $specKey => $specValue) {
                $cutout = intval($specValue);
                $root = substr($this->getLemma(), 0, strlen($this->getLemma()) + $cutout);
                $formsTxt = str_replace($specKey, $root, $formsTxt);
            }
            if (!array_key_exists('_', $specs)) {
                $formsTxt = str_replace('_', $mainForm, $formsTxt);
            }
        }
        $builtForms = json_decode($formsTxt, true);
        return ['ks' => $orderOfKeys, 'fs'=> $builtForms];
    }

    public function getCorrespondingForm(array $params): mixed
    {
        $formsResponse = $this->buildForms();

        if (is_string($formsResponse)) {
            return $formsResponse;
        }

        $orderOfKeys = $formsResponse['ks'];
        $forms       = $formsResponse['fs'];

        $paramsArray = [];
        foreach ($orderOfKeys as $key) {
            $paramsArray[] = $params[$key];
        }
        $object = $forms;
        foreach ($paramsArray as $key)
        {
            $newObject = $object[$key];
            $object = $newObject;
        }
        return $object;
    }
}
