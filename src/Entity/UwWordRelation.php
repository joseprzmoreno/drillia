<?php

namespace App\Entity;

use App\Repository\UwWordRelationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UwWordRelationRepository::class)]
class UwWordRelation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Uw::class)]
    #[ORM\JoinColumn(unique: false, nullable: false)]
    private $uw;

    #[ORM\ManyToOne(targetEntity: Word::class)]
    #[ORM\JoinColumn(unique: false, nullable: false)]
    private $word;

    #[ORM\Column(type: 'integer')]
    private $weight;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUw(): ?Uw
    {
        return $this->uw;
    }

    public function setUw(?Uw $uw): self
    {
        $this->uw = $uw;

        return $this;
    }

    public function getWord(): ?Word
    {
        return $this->word;
    }

    public function setWord(?Word $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }
}
