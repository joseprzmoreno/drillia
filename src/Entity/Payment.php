<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PaymentRepository::class)]
class Payment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'payments')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 2)]
    private $amount;

    #[ORM\Column(type: 'string', length: 255)]
    private $result;

    #[ORM\Column(type: 'string', length: 255)]
    private $method;

    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    private $cardLastDigits;

    #[ORM\Column(type: 'boolean')]
    private $isRecurrent;

    #[ORM\Column(type: 'date', nullable: true)]
    private $nextPaymentDate;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $previousPaymentDate;

    #[ORM\OneToOne(mappedBy: 'payment', targetEntity: Invoice::class, cascade: ['persist', 'remove'])]
    private $invoice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getCardLastDigits(): ?string
    {
        return $this->cardLastDigits;
    }

    public function setCardLastDigits(?string $cardLastDigits): self
    {
        $this->cardLastDigits = $cardLastDigits;

        return $this;
    }

    public function getIsRecurrent(): ?bool
    {
        return $this->isRecurrent;
    }

    public function setIsRecurrent(bool $isRecurrent): self
    {
        $this->isRecurrent = $isRecurrent;

        return $this;
    }

    public function getNextPaymentDate(): ?\DateTimeInterface
    {
        return $this->nextPaymentDate;
    }

    public function setNextPaymentDate(?\DateTimeInterface $nextPaymentDate): self
    {
        $this->nextPaymentDate = $nextPaymentDate;

        return $this;
    }

    public function getPreviousPaymentDate(): ?\DateTimeInterface
    {
        return $this->previousPaymentDate;
    }

    public function setPreviousPaymentDate(?\DateTimeInterface $previousPaymentDate): self
    {
        $this->previousPaymentDate = $previousPaymentDate;

        return $this;
    }

    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }

    public function setInvoice(Invoice $invoice): self
    {
        // set the owning side of the relation if necessary
        if ($invoice->getPayment() !== $this) {
            $invoice->setPayment($this);
        }

        $this->invoice = $invoice;

        return $this;
    }
}
