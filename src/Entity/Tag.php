<?php

namespace App\Entity;

use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TagRepository::class)]
class Tag
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'mainTag', targetEntity: Uw::class)]
    private $uwsWhereMain;

    #[ORM\ManyToMany(targetEntity: Uw::class, mappedBy: 'tags')]
    private $uws;

    #[ORM\ManyToMany(targetEntity: Us::class, mappedBy: 'tags')]
    private $uss;

    public function __construct()
    {
        $this->uwsWhereMain = new ArrayCollection();
        $this->uws = new ArrayCollection();
        $this->uss = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Uw>
     */
    public function getUwsWhereMain(): Collection
    {
        return $this->uwsWhereMain;
    }

    public function addUwsWhereMain(Uw $uwsWhereMain): self
    {
        if (!$this->uwsWhereMain->contains($uwsWhereMain)) {
            $this->uwsWhereMain[] = $uwsWhereMain;
            $uwsWhereMain->setMainTag($this);
        }

        return $this;
    }

    public function removeUwsWhereMain(Uw $uwsWhereMain): self
    {
        if ($this->uwsWhereMain->removeElement($uwsWhereMain)) {
            // set the owning side to null (unless already changed)
            if ($uwsWhereMain->getMainTag() === $this) {
                $uwsWhereMain->setMainTag(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Uw>
     */
    public function getUws(): Collection
    {
        return $this->uws;
    }

    public function addUw(Uw $uw): self
    {
        if (!$this->uws->contains($uw)) {
            $this->uws[] = $uw;
            $uw->addTag($this);
        }

        return $this;
    }

    public function removeUw(Uw $uw): self
    {
        if ($this->uws->removeElement($uw)) {
            $uw->removeTag($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Us>
     */
    public function getUss(): Collection
    {
        return $this->uss;
    }

    public function addUss(Us $uss): self
    {
        if (!$this->uss->contains($uss)) {
            $this->uss[] = $uss;
            $uss->addTag($this);
        }

        return $this;
    }

    public function removeUss(Us $uss): self
    {
        if ($this->uss->removeElement($uss)) {
            $uss->removeTag($this);
        }

        return $this;
    }

    public function __toString() 
    {
        return $this->getName();
    }
}
