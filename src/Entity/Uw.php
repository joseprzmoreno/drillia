<?php

namespace App\Entity;

use App\Repository\UwRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UwRepository::class)]
class Uw
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $lemma;

    #[ORM\Column(type: 'string', length: 510, nullable: true)]
    private $longDefinition;

    #[ORM\Column(type: 'string', length: 255)]
    private $shortDefinition;

    #[ORM\Column(type: 'string', length: 20)]
    private $cat;

    #[ORM\Column(type: 'text', nullable: true)]
    private $info;

    #[ORM\Column(type: 'boolean')]
    private $isBasic;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $properNoun;

    #[ORM\Column(type: 'boolean')]
    private $active;

    #[ORM\Column(type: 'text', nullable: true)]
    private $extra;

    #[ORM\ManyToOne(targetEntity: Tag::class, inversedBy: 'uwsWhereMain')]
    private $mainTag;

    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'uws')]
    private $tags;

    #[ORM\Column(type: 'integer')]
    private $ordering;

    #[ORM\Column(type: 'boolean')]
    private $isInGenerator;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $identifier;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLemma(): ?string
    {
        return $this->lemma;
    }

    public function setLemma(string $lemma): self
    {
        $this->lemma = $lemma;

        return $this;
    }

    public function getLongDefinition(): ?string
    {
        return $this->longDefinition;
    }

    public function setLongDefinition(?string $longDefinition): self
    {
        $this->longDefinition = $longDefinition;

        return $this;
    }

    public function getShortDefinition(): ?string
    {
        return $this->shortDefinition;
    }

    public function setShortDefinition(string $shortDefinition): self
    {
        $this->shortDefinition = $shortDefinition;

        return $this;
    }

    public function getCat(): ?string
    {
        return $this->cat;
    }

    public function setCat(string $cat): self
    {
        $this->cat = $cat;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(?string $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function getIsBasic(): ?bool
    {
        return $this->isBasic;
    }

    public function setIsBasic(bool $isBasic): self
    {
        $this->isBasic = $isBasic;

        return $this;
    }

    public function getProperNoun(): ?string
    {
        return $this->properNoun;
    }

    public function setProperNoun(?string $properNoun): self
    {
        $this->properNoun = $properNoun;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getExtra(): ?string
    {
        return $this->extra;
    }

    public function setExtra(?string $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getMainTag(): ?Tag
    {
        return $this->mainTag;
    }

    public function setMainTag(?Tag $mainTag): self
    {
        $this->mainTag = $mainTag;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function __toString() 
    {
        return empty($this->getIdentifier()) ? str_replace(' ', '_', $this->getLemma() .
            '::' . $this->getCat() . '::' . $this->getShortDefinition()) : $this->getIdentifier();
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(int $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function getIsInGenerator(): ?bool
    {
        return $this->isInGenerator;
    }

    public function setIsInGenerator(bool $isInGenerator): self
    {
        $this->isInGenerator = $isInGenerator;

        return $this;
    }

    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function isAnimated(): bool
    {
         return array_key_exists('anim', $this->getInfo()) &&
             json_decode($this->getInfo(), true)['anim'] == 1;
    }
}
