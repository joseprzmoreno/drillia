<?php

namespace App\Entity;

use App\Repository\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LanguageRepository::class)]
class Language
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 20)]
    private $code;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $origName;

    #[ORM\Column(type: 'string', length: 1000, nullable: true)]
    private $flagUrl;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children')]
    private $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private $children;

    #[ORM\Column(type: 'boolean')]
    private $active;

    #[ORM\Column(type: 'text', nullable: true)]
    private $extra;

    #[ORM\OneToMany(mappedBy: 'language', targetEntity: Paradigm::class, orphanRemoval: true)]
    private $paradigms;

    #[ORM\OneToMany(mappedBy: 'language', targetEntity: Word::class, orphanRemoval: true)]
    private $words;

    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    private $locale;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->paradigms = new ArrayCollection();
        $this->words = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrigName(): ?string
    {
        return $this->origName;
    }

    public function setOrigName(string $origName): self
    {
        $this->origName = $origName;

        return $this;
    }

    public function getFlagUrl(): ?string
    {
        return $this->flagUrl;
    }

    public function setFlagUrl(?string $flagUrl): self
    {
        $this->flagUrl = $flagUrl;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getExtra(): ?string
    {
        return $this->extra;
    }

    public function setExtra(?string $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * @return Collection<int, Paradigm>
     */
    public function getParadigms(): Collection
    {
        return $this->paradigms;
    }

    public function addParadigm(Paradigm $paradigm): self
    {
        if (!$this->paradigms->contains($paradigm)) {
            $this->paradigms[] = $paradigm;
            $paradigm->setLanguage($this);
        }

        return $this;
    }

    public function removeParadigm(Paradigm $paradigm): self
    {
        if ($this->paradigms->removeElement($paradigm)) {
            // set the owning side to null (unless already changed)
            if ($paradigm->getLanguage() === $this) {
                $paradigm->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Word>
     */
    public function getWords(): Collection
    {
        return $this->words;
    }

    public function addWord(Word $word): self
    {
        if (!$this->words->contains($word)) {
            $this->words[] = $word;
            $word->setLanguage($this);
        }

        return $this;
    }

    public function removeWord(Word $word): self
    {
        if ($this->words->removeElement($word)) {
            // set the owning side to null (unless already changed)
            if ($word->getLanguage() === $this) {
                $word->setLanguage(null);
            }
        }

        return $this;
    }

    public function __toString() 
    {
        return $this->getName();
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }
}
