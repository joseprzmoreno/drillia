<?php

namespace App\Entity;

use App\Repository\UsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UsRepository::class)]
class Us
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 1000)]
    private $representation;

    #[ORM\Column(type: 'text')]
    private $tree;

    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'uss')]
    private $tags;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $pretree;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $variations;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $observations;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRepresentation(): ?string
    {
        return $this->representation;
    }

    public function setRepresentation(string $representation): self
    {
        $this->representation = $representation;

        return $this;
    }

    public function getTree(): ?string
    {
        return $this->tree;
    }

    public function setTree(string $tree): self
    {
        $this->tree = $tree;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getPretree(): ?string
    {
        return $this->pretree;
    }

    public function setPretree(?string $pretree): self
    {
        $this->pretree = $pretree;

        return $this;
    }

    public function getVariations(): ?string
    {
        return $this->variations;
    }

    public function setVariations(string $variations): self
    {
        $this->variations = $variations;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }
}
