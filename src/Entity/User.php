<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 64)]
    private $username;

    #[ORM\ManyToOne(targetEntity: Language::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $motherTongue;

    #[ORM\Column(type: 'boolean')]
    private $userActivated;

    #[ORM\Column(type: 'boolean')]
    private $agreeTerms;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $token;

    #[ORM\Column(type: 'datetime')]
    private $creationDate;

    #[ORM\Column(type: 'boolean')]
    private $isVip = false;

    #[ORM\Column(type: 'uuid')]
    private $uuid;

    #[ORM\Column(type: 'boolean')]
    private $passwordRequested;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $company;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $identificationNumber;

    #[ORM\Column(type: 'string', length: 500, nullable: true)]
    private $address;

    #[ORM\Column(type: 'string', length: 25, nullable: true)]
    private $zipCode;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $city;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $country;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $lastPaymentDate;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $automaticRenewal;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $vipEndDate;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $nextRenewalDate;

    #[ORM\Column(type: 'text', nullable: true)]
    private $vipExtraInfo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $region;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Payment::class)]
    private $payments;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $surname;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getMotherTongue(): ?Language
    {
        return $this->motherTongue;
    }

    public function setMotherTongue(?Language $motherTongue): self
    {
        $this->motherTongue = $motherTongue;

        return $this;
    }

    public function getUserActivated(): ?bool
    {
        return $this->userActivated;
    }

    public function setUserActivated(bool $userActivated): self
    {
        $this->userActivated = $userActivated;

        return $this;
    }

    public function getAgreeTerms(): ?bool
    {
        return $this->agreeTerms;
    }

    public function setAgreeTerms(bool $agreeTerms): self
    {
        $this->agreeTerms = $agreeTerms;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function isVip(): bool
    {
        return $this->isVip;
    }

    public function setIsVip(bool $isVip): self
    {
        $this->isVip = $isVip;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getPasswordRequested(): ?bool
    {
        return $this->passwordRequested;
    }

    public function setPasswordRequested(bool $passwordRequested): self
    {
        $this->passwordRequested = $passwordRequested;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(?string $identificationNumber): self
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getLastPaymentDate(): ?\DateTimeInterface
    {
        return $this->lastPaymentDate;
    }

    public function setLastPaymentDate(?\DateTimeInterface $lastPaymentDate): self
    {
        $this->lastPaymentDate = $lastPaymentDate;

        return $this;
    }

    public function getAutomaticRenewal(): ?bool
    {
        return $this->automaticRenewal;
    }

    public function setAutomaticRenewal(?bool $automaticRenewal): self
    {
        $this->automaticRenewal = $automaticRenewal;

        return $this;
    }

    public function getVipEndDate(): ?\DateTimeInterface
    {
        return $this->vipEndDate;
    }

    public function setVipEndDate(?\DateTimeInterface $vipEndDate): self
    {
        $this->vipEndDate = $vipEndDate;

        return $this;
    }

    public function getNextRenewalDate(): ?\DateTimeInterface
    {
        return $this->nextRenewalDate;
    }

    public function setNextRenewalDate(?\DateTimeInterface $nextRenewalDate): self
    {
        $this->nextRenewalDate = $nextRenewalDate;

        return $this;
    }

    public function getVipExtraInfo(): ?string
    {
        return $this->vipExtraInfo;
    }

    public function setVipExtraInfo(?string $vipExtraInfo): self
    {
        $this->vipExtraInfo = $vipExtraInfo;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function isVipProper(): bool
    {
        if (empty($this->getVipEndDate())) {
            return false;
        }

        $today = date("Y-m-d H:i:s");
        $todayDt = new DateTime($today);
        $expireDt = new DateTime($this->getVipEndDate());
        
        $isInVipPeriod = $today <= $expireDt;
        return $this->isVip() && $isInVipPeriod;
    }

    /**
     * @return Collection<int, Payment>
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setUser($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getUser() === $this) {
                $payment->setUser(null);
            }
        }

        return $this;
    }

    public function paymentInfoIsComplete(): bool
    {
        $result = true;
        if ($this->getName() == '' ||
        $this->getSurname() == '' ||
        $this->getIdentificationNumber() == '' ||
        $this->getAddress() == '' ||
        $this->getZipCode() == '' ||
        $this->getCity() == '' ||
        $this->getRegion() == '' ||
        $this->getCountry() == '') { $result = false; }
        return $result;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }
}
