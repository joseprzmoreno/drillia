<?php

$messagesContent = file_get_contents(getcwd() . '/translations_pre/messages.txt');

$lines = explode("\n", $messagesContent);

$cont = 0;
$lineasEn = [];
$lineasEs = [];
$lineasFr = [];
$lineasIt = [];
$lineasDe = [];
$lineasRu = [];

foreach ($lines as $line) {
    switch($cont) {
        case 0:
            if ($line != '') $intro = trim($line);
            break;
        case 1:
            if ($line != '') $lineasEn[] = $intro . ' ' . $line;
            break;
        case 2:
            if ($line != '') $lineasEs[] = $intro . ' ' . $line;
            break;
        case 3:
            if ($line != '') $lineasFr[] = $intro . ' ' . $line;
            break;
        case 4:
            if ($line != '') $lineasIt[] = $intro . ' ' . $line;
            break;
        case 5:
            if ($line != '') $lineasDe[] = $intro . ' ' . $line;
            break;
        case 6:
            if ($line != '') $lineasRu[] = $intro . ' ' . $line;
            break;
    }
    if ($line=='') $cont = 0;
    else $cont++;
}

$translationsDirectory = getcwd() . '/translations/';

$lineasEnTxt = implode("\n", $lineasEn);
file_put_contents($translationsDirectory . 'messages.en.yaml', $lineasEnTxt);

$lineasEsTxt = implode("\n", $lineasEs);
file_put_contents($translationsDirectory . 'messages.es.yaml', $lineasEsTxt);

$lineasFrTxt = implode("\n", $lineasFr);
file_put_contents($translationsDirectory . 'messages.fr.yaml', $lineasFrTxt);

$lineasItTxt = implode("\n", $lineasIt);
file_put_contents($translationsDirectory . 'messages.it.yaml', $lineasItTxt);

$lineasDeTxt = implode("\n", $lineasDe);
file_put_contents($translationsDirectory . 'messages.de.yaml', $lineasDeTxt);

$lineasRuTxt = implode("\n", $lineasRu);
file_put_contents($translationsDirectory . 'messages.ru.yaml', $lineasRuTxt);
