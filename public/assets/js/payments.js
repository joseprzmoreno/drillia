$(function () {

    let result = true;
    const paymentInfoIsCompleteRoute = $('#paymentInfoIsCompleteRoute').val();
    const userUuid = $('#userUuid').val();
    $.ajax(
        {
            url: paymentInfoIsCompleteRoute,
            type: "POST",
            cache: false,
            dataType: "json",
            data: {
                'uuid': userUuid,
            },
            success: function (data) {
                result = data.ans;
                if (result) {
                    $('#paypal_checkout').show();
                    $('#payments_error_need_info_div').hide();
                } else {
                    $('#paypal_checkout').hide();
                    $('#payments_error_need_info_div').show();
                }
            }
        });
});