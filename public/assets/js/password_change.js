$(function () {

    $( "#passwordChangeForm" ).submit(function( event ) {
        event.preventDefault();

        let isValidForm = true;
        let errorMessageDivs = [];
        const userPassword = $('#userPassword').val();
        const userPassword2 = $('#userPassword2').val();
        const passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

        if (!passwordRegex.test(userPassword)) {
            isValidForm = false;
            errorMessageDivs.push('password_error_message');
        }

        if (userPassword !== userPassword2) {
            isValidForm = false;
            errorMessageDivs.push('passwords_must_match');
        }

        if (!isValidForm) {
            let div = errorMessageDivs[0];
            $.notify({message: $('#' + div).html()},{type: 'danger', z_index: 1100});
            return false;
        }        

        $( "#passwordChangeForm" ).unbind().submit();                      
        return true;
      });
    
});