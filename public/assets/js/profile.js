$(function () {

    $( "#profileForm" ).submit(function( event ) {
        event.preventDefault();

        let isValidForm = true;
        let errorMessageDivs = [];
        const route = $('#route').val();
        const userUuid = $('#userUuid').val();
        const username = $('#username').val();
        const userPassword = $('#userPassword').val();
        const userPassword2 = $('#userPassword2').val();
        const motherTongue = $('#motherTongue').val();
        const usernameRegex = /^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$/;
        const passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

        const userFirstname = $('#userFirstname').val();
        const userSurname = $('#userSurname').val();
        const userCompany = $('#userCompany').val();
        const userIdNumber = $('#userIdNumber').val();
        const userAddress = $('#userAddress').val();
        const userZipCode = $('#userZipCode').val();
        const userCity = $('#userCity').val();
        const userRegion = $('#userRegion').val();
        const userCountry = $('#userCountry').val();

        if (!usernameRegex.test(username)) {
            isValidForm = false;
            errorMessageDivs.push('username_error_message');
        }

        if (userPassword != '' && !passwordRegex.test(userPassword)) {
            isValidForm = false;
            errorMessageDivs.push('password_error_message');
        }

        if (userPassword != '' && userPassword !== userPassword2) {
            isValidForm = false;
            errorMessageDivs.push('passwords_must_match');
        }

        if (!isValidForm) {
            let div = errorMessageDivs[0];
            $.notify({message: $('#' + div).html()},{type: 'danger', z_index: 1100});
            return false;
        }        

        $.ajax(
            {
                url: route,
                type: "POST",
                cache: false,
                dataType: "json",
                data: {
                    'uuid': userUuid,
                    'username': username,
                    'password': userPassword,
                    'motherTongue': motherTongue,
                    'name': userFirstname,
                    'surname': userSurname,
                    'company': userCompany,
                    'idNumber': userIdNumber,
                    'address': userAddress,
                    'zipCode': userZipCode,
                    'city': userCity,
                    'region': userRegion,
                    'country': userCountry
                },
                success: function (data) {
                    if (data['ans'] == '0') {
                        $.notify({message: $('#profile_error_saving').html()},{type: 'danger', z_index: 1100});
                        return false;
                    } else {
                        $.notify({message: $('#profile_saved_correctly').html()},{type: 'info', z_index: 1100});
                        return false;
                    }
                }
            });

        return false;
      });
    
});