$(function () {

    $( "#registerForm" ).submit(function( event ) {
        event.preventDefault();

        let isValidForm = true;
        let errorMessageDivs = [];
        const username = $('#username').val();
        const userEmail = $('#userEmail').val();
        const userPassword = $('#userPassword').val();
        const userPassword2 = $('#userPassword2').val();
        const motherTonge = $('#motherTongue').val();
        const acceptTerms = $('#acceptTerms').val();
        const usernameRegex = /^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$/;
        const passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

        if (!usernameRegex.test(username)) {
            isValidForm = false;
            errorMessageDivs.push('username_error_message');
        }

        if (!passwordRegex.test(userPassword)) {
            isValidForm = false;
            errorMessageDivs.push('password_error_message');
        }

        if (userPassword !== userPassword2) {
            isValidForm = false;
            errorMessageDivs.push('passwords_must_match');
        }

        if (!isValidForm) {
            let div = errorMessageDivs[0];
            $.notify({message: $('#' + div).html()},{type: 'danger', z_index: 1100});
            return false;
        }        

        $.ajax(
            {
                url: "check_username_and_email",
                type: "POST",
                cache: false,
                dataType: "json",
                data: {'username': $('#username').val(), 'mail': $('#userEmail').val()},
                success: function (data) {
                    if (data['ans'] == 1) {
                        $.notify({message: $('#register_error_username_already_exists').html()},{type: 'danger', z_index: 1100});
                        return false;
                    } else if (data['ans'] == 2) {
                        $.notify({message: $('#register_error_email_already_exists').html()},{type: 'danger', z_index: 1100});
                        return false;
                    } else {
                        $( "#registerForm" ).unbind().submit();                      
                        return true;
                    }
                }
            });

        return false;
      });
    
});