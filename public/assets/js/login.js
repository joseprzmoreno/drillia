$(function () {

    function validateEmail(email) {
        return String(email)
          .toLowerCase()
          .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          );
      };

    $( "#btn_recover_password" ).click(function( event ) {
            event.preventDefault();
            let email = $('#inputEmail').val();
            if (!validateEmail(email)) {
                $.notify({message: $('#enter_a_valid_email').html()},{type: 'danger', z_index: 1100});
                return false;
            }

            $.ajax(
              {
                  url: $(this).attr("href"),
                  type: "POST",
                  cache: false,
                  dataType: "json",
                  data: {'email': email},
                  success: function (data) {
                      if (data['ans'] == 1) {
                          $.notify({message: $('#recover_password_no_such_email').html()},{type: 'danger', z_index: 1100});
                          return false;
                      } else if (data['ans'] == 2) {
                          $.notify({message: $('#recover_password_already_requested').html()},{type: 'danger', z_index: 1100});
                          return false; 
                      } else {
                        $.notify({message: $('#recover_password_email_sent').html()},{type: 'info', z_index: 1100});                  
                          return true;
                      }
                  }
              });

      });
    
});